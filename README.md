# Nekos.life API Wrapper for Python (But not using Nekos.life API)

The module that gets some Neko image URL.

## Examples

```python
import nyathon
print(nyathon.GetNeko()) # Prints out "https://cdn.nekos.life/neko/neko_082.jpg"
```

## Dependencies/Requirements

1. bs4
2. requests
