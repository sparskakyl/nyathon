#!/bin/env python3

# Nekos Life API Wrapper for Python

from bs4 import BeautifulSoup # For scraping to get the Image URL.
import requests # For getting HTML shizzles

def GetNeko(*args,**kwargs):
	BaseURL = "https://nekos.life/" # Set the URL.
	LewdPLZ = kwargs.get("lewd", False)
	if LewdPLZ == True: BaseURL = "https://nekos.life/lewd" # If they wants NSFW, Set the URL again.
	Source = requests.get(BaseURL).text # Get some HTML shizzles.
	Soup = BeautifulSoup(Source, features="html.parser") # BeautifulSoup-ify some shizzles.
	Neko = Soup.find("img", alt="neko")["src"] # Find the image with alt = "neko" and get the URL.
	if Neko == None: raise Exception("Neko not found!") # If it does not exists, raise error.
	return str(Neko) # Return the URL.

if __name__ == "__main__":
	raise Exception("This is module!") # If someone runs this module, raise error.
